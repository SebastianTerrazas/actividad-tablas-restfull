# Actividad | Tablas RESTfull

## Sebastián Terrazas Sandoval

Matrícula: 320808

## How it works:


    GET /results/:n1/:n2 -> Sumar n1 + n2

    POST /results/ -> Multiplicar n1 * n2

    PUT /results/ -> Dividir n1 / n2

    PATCH /results/ -> Potencia n1 ^ n2
    
    DELETE /results/:n1/:n2 -> restar n1 - n2
