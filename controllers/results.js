const express = require('express');

function add(req, res, next) {
    let n1 = req.params.n1;
    let n2 = req.params.n2;
    let sum = +n1 + +n2;

    res.send(`La suma de ${n1} y ${n2} es: ${sum}`);
}

function multiply(req, res, next) {
    let n1 = req.body.n1;
    let n2 = req.body.n2;
    let product = +n1 * +n2;

    res.send(`La multiplicación de ${n1} y ${n2} es: ${product}`);
}

function divide(req, res, next) {
    let n1 = req.body.n1;
    let n2 = req.body.n2;
    let cociente = +n1 / +n2;

    res.send(`La división de ${n1} y ${n2} es: ${cociente}`);
}

function elevate(req, res, next) {
    let n1 = req.body.n1;
    let n2 = req.body.n2;
    let elevated = Math.pow(+n1, +n2);

    res.send(`La potencia de ${n1} a ${n2} es: ${elevated}`);
}

function subtract(req, res, next) {
    let n1 = req.params.n1;
    let n2 = req.params.n2;
    let deduct = +n1 - +n2;

    res.send(`La resta de ${n1} y ${n2} es: ${deduct}`);
}

module.exports = {
    add, multiply, divide, elevate, subtract
}