const express = require('express');
const controller = require('../controllers/results');
const router = express.Router();

// GET /results/:n1/:n2 -> Sumar n1 + n2
router.get('/:n1/:n2', controller.add);
// POST /results/ -> Multiplicar n1 * n2
router.post('/', controller.multiply);
// PUT /results/ -> Dividir n1 / n2
router.put('/', controller.divide);
// PATCH /results/ -> Potencia n1 ^ n2
router.patch('/', controller.elevate);
// DELETE /results/:n1/:n2 -> restar n1 - n2
router.delete('/:n1/:n2', controller.subtract);

module.exports = router;